import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Top from './components/pages/top';
import Work from './components/pages/work';
import Contact from './components/pages/contact';
import Donate from './components/pages/donate';
import Footer from './components/organisms/footer';

const Index = () => {
  return (
    <React.Fragment>
      <Router>
        <Route exact path="/" component={Top} />
        <Route exact path="/work" component={Work} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/donate" component={Donate} />
      </Router>
      <Footer />
    </React.Fragment>
  );
};

ReactDOM.render(<Index />, document.getElementById('root'));
