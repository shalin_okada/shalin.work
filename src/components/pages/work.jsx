import React from 'react';
import { withStyles } from '@material-ui/core';
import Header from '../organisms/header';
import WorkCardList from '../organisms/workCardList';

const workData = [
  {
    title: 'hoge',
    desc: 'fuga',
    image: 'https://fukatsu.tech/wp-content/uploads/2018/06/reactjs-1280x720.png',
  },
  {
    title: 'hoge',
    desc: 'fuga',
    image: 'https://fukatsu.tech/wp-content/uploads/2018/06/reactjs-1280x720.png',
  },
  {
    title: 'hoge',
    desc: 'fuga',
    image: 'https://fukatsu.tech/wp-content/uploads/2018/06/reactjs-1280x720.png',
  },
  {
    title: 'hoge',
    desc: 'fuga',
    image: 'https://fukatsu.tech/wp-content/uploads/2018/06/reactjs-1280x720.png',
  },
  {
    title: 'hoge',
    desc: 'fuga',
    image: 'https://fukatsu.tech/wp-content/uploads/2018/06/reactjs-1280x720.png',
  },
];

const Work = (props) => {
  const { classes } = props;
  return (
    <div className={classes.work}>
      <Header />
      <WorkCardList workData={workData} />
    </div>
  );
};

const styles = {
  work: {
    marginBottom: 30,
  },
};

export default withStyles(styles)(Work);
