import React from 'react';
import { withStyles } from '@material-ui/core';
import Header from '../organisms/header';

const Top = (props) => {
  const { classes } = props;
  return (
    <div className={classes.top}>
      <Header />
    </div>
  );
};

const styles = {
  top: {
    height: '100vh',
    width: '100vw',
    '& > div': {
      height: '100vh',
      '& > div': {
        height: '50vh',
      },
    },
  },
};

export default withStyles(styles)(Top);
