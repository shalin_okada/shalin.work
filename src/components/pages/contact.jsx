import React from 'react';
import { withStyles } from '@material-ui/core';
import Header from '../organisms/header';
import ContactList from '../organisms/contactList';

const contactListData = [
  {
    text: 'Twitter',
    link: 'https://twitter.com/shalin_okada',
  },
  {
    text: 'Facebook',
    link: 'https://www.facebook.com/shalin.okada',
  },
  {
    text: 'GitHub',
    link: 'https://github.com/shalin-okada',
  },
  {
    text: 'Bitbucket',
    link: 'https://github.com/shalin-okada',
  },
  {
    text: 'Qiita',
    link: 'https://qiita.com/shalin',
  },
  {
    text: 'Mail',
    link: 'mailto:shalinokada@gmail.com',
  },
];

const Contact = () => {
  return (
    <div>
      <Header />
      <ContactList contactListData={contactListData} />
    </div>
  );
};

export default withStyles()(Contact);
