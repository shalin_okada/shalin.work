import React from 'react';
import { withStyles } from '@material-ui/core';
import Header from '../organisms/header';

const Donate = (props) => {
  const { classes } = props;
  return (
    <div className={classes.top}>
      <Header />
      <img className={classes.kyash} src="https://s3-ap-northeast-1.amazonaws.com/s3.shalin.work/kyash.jpg" alt="kyash" />
    </div>
  );
};

const styles = {
  top: {
    textAlign: 'center',
  },
  kyash: {
    width: '50vw',
    margin: '50px auto',
  },
};

export default withStyles(styles)(Donate);
