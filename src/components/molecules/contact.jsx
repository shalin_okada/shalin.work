import React from 'react';
import { withStyles, Typography } from '@material-ui/core';

const Contact = (props) => {
  const { classes, data } = props;
  return (
    <a className={classes.contact} href={data.link}>
      <Typography>
        {data.text}
      </Typography>
    </a>

  );
};

const styles = {
  contact: {
    fontFamily: 'Arial-Black',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    margin: '30px 0',
    color: 'white',
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'none',
    },
    '&:visited': {
      textDecoration: 'none',
    },
    '& p': {
      fontSize: '5vw',
      fontWeight: 'bold',
    },
  },
};

export default withStyles(styles)(Contact);
