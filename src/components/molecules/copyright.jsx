import React from 'react';
import {
  withStyles,
  Typography,
} from '@material-ui/core';

const Copyright = (props) => {
  const { classes } = props;
  return (
    <Typography className={classes.copyright}>
      © 2019 shalin.work
    </Typography>
  );
};

const styles = {
  copyright: {
    color: '#FFCA28',
    fontSize: 20,
    fontWeight: 'bold',
  },
};

export default withStyles(styles)(Copyright);
