import React from 'react';
import {
  withStyles,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  CardActions,
  Typography,
  Button,
} from '@material-ui/core';

const WorkCard = (props) => {
  const { data, classes } = props;
  console.log(data.image);
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          className={classes.image}
          title="thumbnail"
          image={data.image}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {data.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {data.desc}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          DETAIL
        </Button>
      </CardActions>
    </Card>
  );
};

const styles = {
  card: {
    width: 256,
  },
  image: {
    width: 256,
    height: 144,
  },
};

export default withStyles(styles)(WorkCard);
