import React from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core';

const Nav = (props) => {
  const { classes } = props;
  return (
    <div className={classes.nav}>
      <Link to="/work" className={classes.navChild}>
        WORK
      </Link>
      <Link to="/contact" className={classes.navChild}>
        CONTACT
      </Link>
      <Link to="/donate" className={classes.navChild}>
        DONATE
      </Link>
    </div>
  );
};

const styles = {
  nav: {
    fontFamily: 'Arial-Black',
    fontSize: '3vw',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    '& a': {
      margin: '0 3vw',
      textStroke: '1px #BA68C8',
      color: 'transparent',
      textDecoration: 'none',
      transitionDuration: '0.2s',
      '&:hover': {
        textDecoration: 'none',
        color: 'rgba(156, 39, 176, 0.5)',
      },
      '&:visited': {
        textDecoration: 'none',
      },
    },
  },
};

export default withStyles(styles)(Nav);
