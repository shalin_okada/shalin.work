import React from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core';

const Logo = (props) => {
  const { classes } = props;
  return (
    <div className={classes.logo}>
      <Link to="/">
        <img src="https://s3-ap-northeast-1.amazonaws.com/s3.shalin.work/logo.png" alt="ロゴ" />
      </Link>
    </div>
  );
};

const styles = {
  logo: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    '& img': {
      width: '100%',
    },
  },
};

export default withStyles(styles)(Logo);
