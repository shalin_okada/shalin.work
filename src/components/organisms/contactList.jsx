import React from 'react';
import { withStyles } from '@material-ui/core';
import Contact from '../molecules/contact';

const createContactList = (contactListData) => {
  const contactList = [];
  contactListData.forEach((data, index) => {
    contactList.push(<Contact key={index} data={data} />);
  });
  return contactList;
};

const ContactList = (props) => {
  const { contactListData } = props;
  const contactList = createContactList(contactListData);
  console.log(contactList);
  return (
    <div>
      {contactList}
    </div>
  );
};

export default withStyles()(ContactList);
