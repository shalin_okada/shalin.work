import React from 'react';
import {
  withStyles,
  Grid,
} from '@material-ui/core';
import WorkCard from '../molecules/workCard';

const createCardList = (workData) => {
  const cardList = [];
  workData.forEach((data, index) => {
    const card = (
      <Grid key={index} item>
        {console.log('hoge')}
        <WorkCard data={data} />
        {console.log('fuga')}
      </Grid>
    );
    cardList.push(card);
  });
  return cardList;
};

const WorkCardList = (props) => {
  const { workData, classes } = props;
  const cardList = createCardList(workData);

  return (
    <Grid container className={classes.workCardList} spacing={2} justify="center">
      {cardList}
    </Grid>
  );
};

const styles = {
  workCardList: {
    width: '80%',
    margin: '0 auto',
  },
};

export default withStyles(styles)(WorkCardList);
