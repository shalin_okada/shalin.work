import React from 'react';
import { withStyles } from '@material-ui/core';
import Logo from '../molecules/logo';
import Nav from '../molecules/nav';

const Header = (props) => {
  const { classes } = props;
  return (
    <div className={classes.header}>
      <Logo />
      <Nav />
    </div>
  );
};

const styles = {
  header: {
    width: '60%',
    margin: '0 auto',
  },
};

export default withStyles(styles)(Header);
