import React from 'react';
import { withStyles } from '@material-ui/core';
import Copyright from '../molecules/copyright';

const Footer = (props) => {
  const { classes } = props;
  return (
    <div className={classes.footer}>
      <Copyright />
    </div>
  );
};

const styles = {
  footer: {
    width: '100%',
    textAlign: 'center',
    bottom: 0,
  },
};

export default withStyles(styles)(Footer);
